package com.domain;

/**
 * @author Emanuel Rasmusson {@literal <mailto:emanuelrasmusson@gmail.com/>}
 */

public class Prospect {

    private final String customerName;
    private final double loanAmount;
    private final double interest;
    private final int years;
    private double monthlyPayment;

    public Prospect(String customerName, double loanAmount, double interest, int years) {
        this.customerName = customerName;
        this.loanAmount = loanAmount;
        this.interest = interest;
        this.years = years;
        this.monthlyPayment = 0;
    }


    public String getCustomerName() {
        return customerName;
    }

    public double getLoan() {
        return loanAmount;
    }


    public int getYear() {
        return years;
    }

    public double getMonthlyPayment(){
        return monthlyPayment;
    }

    public double getInterest() {
        return interest;
    }
    public void setMonthlyPayment(){
        double result = 0;
        double percentage = interest/100/12;// monthly interest rate
        double tempNumber = (1 + percentage);
        double base = tempNumber;
        for (int i= 0; i < years * 12; i++) {
            if(i > 0) {
                tempNumber = tempNumber * base;
            }
        }
        result = loanAmount * ((percentage * tempNumber)/( tempNumber - 1));
        this.monthlyPayment = result;
    }


    public double setMonthlyPayment(double loanAmount, int years, double interest) {
        double result = 0;
        double percentage = interest/100/12;// monthly interest rate
        double tempNumber = (1 + percentage);
        double base = tempNumber;
        for (int i= 0; i < years * 12; i++) {
            if(i > 0) {
                tempNumber = tempNumber * base;
            }
        }
        result = loanAmount * ((percentage * tempNumber)/( tempNumber - 1));
        this.monthlyPayment = result;
        return result;
    }
}




