package com.integration;

import com.domain.Prospect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Emanuel Rasmusson {@literal <mailto:emanuelrasmusson@gmail.com/>}
 */


public class ProspectService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProspectService.class);

    private List<Prospect> prospectList;

    public ProspectService() {
        this.prospectList = new ArrayList<>();
    }

    public void appendProspect(Prospect prospect) { prospectList.add(prospect); }

    public void getDataFromFile(File file) {
        // If its more signs in file we use signChanger
        String signChanger;
        int counter = 0;
        String prospect = null;
        try {
            LocalDateTime localDateTimeStart = LocalDateTime.now();
            // Uses Scanner instead of bufferreader because im not working with multiple threads,
            // ofcourse are bufferreader faster but didnt need it in this test
            Scanner inputFile = new Scanner(file);
            inputFile.useDelimiter(",");
            // iterate through the file line by line

            //logging first row out of the loop!
            LOGGER.info(inputFile.nextLine());
            while (inputFile.hasNextLine()) {
                //
                if (prospect != null ) {
                    inputFile.nextLine();
                }
                String string = inputFile.nextLine();
                Scanner findSign = new Scanner(string);
                findSign.useDelimiter(",");
                String findName;
                // Count the sign
                counter = string.length() - string.replace(",", "").length();
                if (counter == 3) {
                    findName = findSign.next();
                    appendProspect(findName, findSign);
                } else if (counter >= 4) {
                    signChanger = findSign.next() + " " + findSign.next();
                    findName = signChanger.replace("\"", "");
                    appendProspect(findName, findSign);

                }

            }
            LOGGER.info(String.valueOf(localDateTimeStart));
            LOGGER.info("getFileData is done."+ inputFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOGGER.error("Failed inputFile", e);
        }
    }
    private void appendProspect(String name, Scanner findSign) {
        double loan;
        double interest;
        int years;
        loan = Double.parseDouble(findSign.next());
        interest = Double.parseDouble(findSign.next());
        years = Integer.parseInt(findSign.next());
        Prospect prospect = new Prospect(name, loan, interest, years);
        //The value of the monthly payment
        prospect.setMonthlyPayment();
        appendProspect(prospect);
    }

    public void iterateList(int numDigits) {
        //How the output should be formatted
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(numDigits);
        for (int i = 0; i < prospectList.size(); i++) {
            System.out.println("Output prospect " + (i + 1) + ": " + prospectList.get(i).getCustomerName()
                    + " wants to borrow " + prospectList.get(i).getLoan() + "€ for a period of "
                    + prospectList.get(i).getYear() + " years and pay " + nf.format(prospectList.get(i).getMonthlyPayment()) + "€ for each month");
        }
}

}
