package com.mortageplanmoneybin.demo;
import com.integration.ProspectService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;


@SpringBootApplication
public class ProspectApplication {


    public static void main(String[] args) throws IOException {

        ProspectService test = new ProspectService();
        File file = new File("C:\\dev\\mortageplan\\src\\test\\java\\com\\mortageplanmoneybin\\demo\\prospects.txt");
        test.getDataFromFile(file);
        test.iterateList(2);

        SpringApplication.run(ProspectApplication.class, args);
    }


    }

