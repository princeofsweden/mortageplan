package com.mortageplanmoneybin.demo;


import com.domain.Prospect;
import com.integration.ProspectService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


import java.io.File;

/**
 * @author Emanuel Rasmusson {@literal <mailto:emanuelrasmusson@gmail.com/>}
 */


@SpringBootTest
class ProspectsTest {

    @Test
    void setup()  {

        ProspectService test = new ProspectService();
        File file = new File("C:\\dev\\mortageplan\\src\\test\\java\\com\\mortageplanmoneybin\\demo\\prospects.txt");
        test.getDataFromFile(file);
        test.iterateList(3);
    }

     @Test
     void firstProspectTest() {
        Prospect prospect = new Prospect("Juha", 1000, 5, 2);
         double calculate = prospect.getLoan();
         double calculate1 = prospect.getMonthlyPayment();
         double calculate2 = prospect.getInterest();
         double calculate3 = prospect.getYear();
         String name = prospect.getCustomerName();
         Assertions.assertEquals("Juha",name);
         Assertions.assertEquals(1000, calculate);
         Assertions.assertEquals(0, calculate1);
         Assertions.assertEquals(5, calculate2);
         Assertions.assertEquals(2, calculate3);
        }
    @Test
    void secondProspectTest() {
        Prospect prospect = new Prospect("Karvinen", 4356, 1.27, 6);
        double calculate = prospect.getLoan();
        double calculate1 = prospect.getMonthlyPayment();
        double calculate2 = prospect.getInterest();
        double calculate3 = prospect.getYear();
        String name = prospect.getCustomerName();
        Assertions.assertEquals("Karvinen",name);
        Assertions.assertEquals(4356, calculate);
        Assertions.assertEquals(0, calculate1);
        Assertions.assertEquals(1.27, calculate2);
        Assertions.assertEquals(6, calculate3);
    }
    @Test
    void thirdProspectTest() {
        Prospect prospect = new Prospect("Claes Månsson", 1300.55, 8.67, 2);
        double calculate = prospect.getLoan();
        double calculate1 = prospect.getMonthlyPayment();
        double calculate2 = prospect.getInterest();
        double calculate3 = prospect.getYear();
        String name = prospect.getCustomerName();
        Assertions.assertEquals("Claes Månsson",name);
        Assertions.assertEquals(1300.55, calculate);
        Assertions.assertEquals(0, calculate1);
        Assertions.assertEquals(8.67, calculate2);
        Assertions.assertEquals(2, calculate3);
    }
    @Test
    void fourthProspectTest() {
        Prospect prospect = new Prospect("Clarencé,Andersson", 2000, 6, 4);
        double calculate = prospect.getLoan();
        double calculate1 = prospect.getMonthlyPayment();
        double calculate2 = prospect.getInterest();
        double calculate3 = prospect.getYear();
        String name = prospect.getCustomerName();
        Assertions.assertEquals("Clarencé,Andersson",name);
        Assertions.assertEquals(2000, calculate);
        Assertions.assertEquals(0, calculate1);
        Assertions.assertEquals(6, calculate2);
        Assertions.assertEquals(4, calculate3);
    }

    @Test
    void  prospectFirstTest() {
        Prospect prospect = new Prospect("Juha", 1000, 5, 2);
        double f = prospect.setMonthlyPayment(prospect.getLoan(), prospect.getYear(), prospect.getInterest());
        Assertions.assertEquals(43.87138973406859,f);
    }
    @Test
    void  prospectSecondTest() {
        Prospect prospect = new Prospect("Karvinen", 4356, 1.27, 6);
        double f = prospect.setMonthlyPayment(prospect.getLoan(), prospect.getYear(), prospect.getInterest());
        Assertions.assertEquals(62.86631476623255,f);
    }
    @Test
    void  prospectThirdTest() {
        Prospect prospect = new Prospect("Claes Månsson", 1300.55, 8.67, 2);
        double f = prospect.setMonthlyPayment(prospect.getLoan(), prospect.getYear(), prospect.getInterest());
        Assertions.assertEquals(59.21856882868132,f);
    }
    @Test
    void  prospectFourthTest() {
        Prospect prospect = new Prospect("Clarencé,Andersson", 2000, 6, 4);
        double f = prospect.setMonthlyPayment(prospect.getLoan(), prospect.getYear(), prospect.getInterest());
        Assertions.assertEquals(46.970058095872126,f);
    }
}
